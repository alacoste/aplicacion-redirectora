import socket
import random


if __name__ == '__main__':
    #.. lista de URLs
    urls = ["http://cursosweb.github.io/", "http://www.google.com/", "http://www.facebook.com/"
            , "http://www.gsyc.es/", "http://www.youtube.com/", "http://www.twitter.com/", "http://www.wikipedia.org/"]

    # AF_INET: Internet Protocol version 4. SOCK_STREAM: TCP
    mysocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

    # Arreglar lo del puerto ocupado por el servidor (opcional)
    #mysocket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)

    # eliminando el localhost podemos recibir conexiones desde cualquier lugar
    PORT = 3333
    mysocket.bind(('localhost', PORT))
    mysocket.listen(5) #.. 5 conexiones simultáneas máximo TCP

    #.. aplicación redirectora
    try:
        while True:
            url = random.choice(urls)
            print(f"Esperando clientes en el puerto {PORT}")
            clientsocket, addr = mysocket.accept()
            print("Conexión establecida con: ", addr)
            recibido = clientsocket.recv(2048)
            print("Mensaje recibido: \n", recibido)
            redirect = "HTTP/1.1 302 Moved Temporarily\r\nLocation: " + url + "\r\n\r\n"
            #Codificación de la respuesta para el cliente
            clientsocket.send(redirect.encode('utf-8'))
            clientsocket.close()
    except KeyboardInterrupt:
        print("closing server...")
        mysocket.close()
